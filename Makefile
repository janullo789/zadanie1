CC=gcc
CFLAGS=-Wall
LIBS=-lm 

main: main.o srednia.o mediana.o odchylenie.o
	$(CC) $(CFLAGS) -o main main.o srednia.o mediana.o odchylenie.o $(LIBS)

main.o: main.c funkcje.h
	$(CC) $(CFLAGS) -c main.c

odchylenie.o: odchylenie.c
	$(CC) $(CFLAGS) -c odchylenie.c

mediana.o: mediana.c
	$(CC) $(CFLAGS) -c mediana.c

srednia.o: srednia.c
	$(CC) $(CFLAGS) -c srednia.c