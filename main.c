#include <stdlib.h>
#include <stdio.h>
#include "funkcje.h"

void readrec(FILE *file, float *p_tabx, float *p_taby, float *p_tabRHO);

int main()
{
    FILE *plik;

    float *tabx;
    float *taby;
    float *tabRHO;

    tabx = (float*)malloc(50 * sizeof(float));
    taby = (float*)malloc(50 * sizeof(float));
    tabRHO = (float*)malloc(50 * sizeof(float));

    if ((plik = fopen("P0001_attr.rec", "a+")) == NULL)
        printf("Blad otwarcia pliku.\n");
    else
    {
        readrec(plik, tabx, taby, tabRHO);
        fprintf(plik, "Dane dla X:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", srd(tabx), med(tabx), odch(tabx));
        fprintf(plik, "Dane dla Y:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", srd(taby), med(taby), odch(taby));
        fprintf(plik, "Dane dla RHO:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", srd(tabRHO), med(tabRHO), odch(tabRHO));
    }

    printf("Dane dla X:\nSrednia: %f\n", srd(tabx));
    printf("Mediana: %f\n", med(tabx));
    printf("Odchylenie standardowe: %f\n", odch(tabx));

    printf("Dane dla Y:\nSrednia: %f\n", srd(taby));
    printf("Mediana: %f\n", med(taby));
    printf("Odchylenie standardowe: %f\n", odch(taby));

    printf("Dane dla RHO:\nSrednia: %f\n", srd(tabRHO));
    printf("Mediana: %f\n", med(tabRHO));
    printf("Odchylenie standardowe: %f\n", odch(tabRHO));

    free(tabx);
    free(taby);
    free(tabRHO);

    fclose(plik);

    return 0;
}

void readrec(FILE *file, float *p_tabx, float *p_taby, float *p_tabRHO)
{
    char lp[4] = "";
    int i = 0;
    
    while(fscanf(file, "%s %f %f %f", lp, &p_tabx[i], &p_taby[i], &p_tabRHO[i])>0)
        {
            printf("%s %f %f %f \n", lp, p_tabx[i], p_taby[i], p_tabRHO[i]);
            i++;
        }
    
    return;
}
